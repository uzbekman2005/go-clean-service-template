package main

import (
	"gitlab.com/uzbekman2005/go-clean-service-template/config"
	"gitlab.com/uzbekman2005/go-clean-service-template/internal/app"
)

func main() {
	// Configuration
	cfg := config.LoadConfig()

	// Run
	app.Run(cfg)
}
