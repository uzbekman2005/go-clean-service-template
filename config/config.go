package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment               string
	PostgresHost              string
	PostgresPort              string
	PostgresUser              string
	PostgresPassword          string
	PostgresDatabase          string
	LogLevel                  string
	PGXPoolMax                int
	TemplateServicePort       string
	RabbitMQHost              string
	RabbitMQPort              string
	RabbitMQUser              string
	RabbitMQPassword          string
	RabbitMQConnectionTry     int
	RabbitMQConnectionTimeOut int
}

func LoadConfig() *Config {
	c := &Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop")) // develop, staging, production
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "business_service"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))
	c.PGXPoolMax = cast.ToInt(GetOrReturnDefault("PGX_POOL_MAX", 2))

	c.TemplateServicePort = cast.ToString(GetOrReturnDefault("TEMPLATE_SERVICE_PORT", "9999"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
