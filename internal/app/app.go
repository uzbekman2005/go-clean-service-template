// Package app configures and runs application.
package app

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/uzbekman2005/go-clean-service-template/config"
	"gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/service"
	grpcclient "gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/service/grpcClient"
	ts "gitlab.com/uzbekman2005/go-clean-service-template/internal/genproto/template_service"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/db"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Run creates objects via constructors.
func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)

	// Repository
	// postgres://user:password@host:5432/database
	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPoolMax))
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
	defer pg.Close()

	clients, err := grpcclient.New(*cfg)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	templateService := service.NewTemplateService(l, clients, pg)

	lis, err := net.Listen("tcp", ":"+cfg.TemplateServicePort)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	ts.RegisterTemplateServiceServer(c, templateService)

	l.Info("Server is running on" + "port" + ": " + cfg.TemplateServicePort)

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: ", err)
	}
}
