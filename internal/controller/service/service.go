package service

import (
	grpcclient "gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/service/grpcClient"
	"gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/storage"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/db"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/logger"
)

type TemplateService struct {
	Logger  *logger.Logger
	Clients grpcclient.Clients
	Storage storage.IStorage
}

func NewTemplateService(l *logger.Logger, client grpcclient.Clients, stg *db.Postgres) *TemplateService {
	return &TemplateService{
		Logger:  l,
		Clients: client,
		Storage: storage.NewStoragePg(stg),
	}
}
