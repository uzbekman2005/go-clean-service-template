package postgres

import (
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/db"
)

type TemplateRepo struct {
	Db *db.Postgres
}

func NewTemplateRepo(pDb *db.Postgres) *TemplateRepo {
	return &TemplateRepo{Db: pDb}
}
