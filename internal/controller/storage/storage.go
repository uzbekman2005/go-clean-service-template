package storage

import (
	"gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/storage/postgres"
	"gitlab.com/uzbekman2005/go-clean-service-template/internal/controller/storage/repo"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/db"
)

type IStorage interface {
	Template() repo.TemplateStorageI
}

type StoragePg struct {
	Db           *db.Postgres
	TemplateRepo repo.TemplateStorageI
}

// NewStoragePg
func NewStoragePg(pDb *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:           pDb,
		TemplateRepo: postgres.NewTemplateRepo(pDb),
	}
}

func (s StoragePg) Template() repo.TemplateStorageI {
	return s.TemplateRepo
}
