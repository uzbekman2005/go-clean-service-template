package rabbitmq

import (
	"github.com/streadway/amqp"
	"gitlab.com/uzbekman2005/go-clean-service-template/config"
	"gitlab.com/uzbekman2005/go-clean-service-template/pkg/logger"
	messagebroker "gitlab.com/uzbekman2005/go-clean-service-template/pkg/message_broker"
)

type RabbitMQ struct {
	Chan *amqp.Channel
	Log  *logger.Logger
}

func New(cfg *config.Config, log *logger.Logger) *RabbitMQ {
	conn, err := messagebroker.NewRabbitMQ(cfg)
	if err != nil {
		panic(err)
	}

	return &RabbitMQ{
		Chan: conn.Producer,
		Log:  log,
	}
}
